# include <iostream>
using namespace std;

int main ()
{
float verbrauch_km = 0 , verbrauch_liter = 0 , verbrauch;

cout  << " Berechnung des Durchschnittsverbrauchs" << endl;
do {
  cout  << " verbrauchter Kraftstoff in l: ";
  cin  >> verbrauch_liter ;
} while(verbrauch_liter == 0);

do {
 cout << " gefahrene Strecke in km: ";
 cin  >> verbrauch_km ;
} while(verbrauch_km == 0);

verbrauch = ( verbrauch_liter / verbrauch_km ) * 100 ;

cout << " Der Durchschnittsverbrauch ist " << verbrauch << " l/100km " ;

return 0;
}
