 # include <iostream>
 using namespace std;

 int main ()
 {
 int iZahl1 , iZahl2 , iErgebnis ;

cout << " Widerstands - Parallelschaltung" << endl;
 cout << " R1 in Ohm: ";
 cin >> iZahl1 ;

 cout << " R2 in Ohm: ";
 cin >> iZahl2 ;

 iErgebnis = ( iZahl1 * iZahl2 ) / ( iZahl1 + iZahl2 ) ;

 cout << " Der Gesamtwiderstand ist " << iErgebnis << " Ohm." ;

 return 0;
 }
