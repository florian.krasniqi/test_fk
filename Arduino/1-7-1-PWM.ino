const int potiPin = A0;
const int ledPin = 6;

 

int sensor = 0;
int pwm = 0;

 

void setup() {
  Serial.begin(9600);
}

 

void loop() {
  sensor = analogRead(potiPin);
  pwm = map(sensor, 0, 1023, 0, 255);
  analogWrite(ledPin, pwm);

 

  Serial.print("Sensor = ");
  Serial.print(sensor);
  Serial.print(" PWM Ausgabe = ");
  Serial.println(pwm);
  delay(10);
}
