int rot = 12;
int gelb = 8;
int gruen = 10;

 

void setup() {
  pinMode(rot, OUTPUT);
  pinMode(gelb, OUTPUT);
  pinMode(gruen, OUTPUT);
}

 

void loop() {
  digitalWrite(rot, HIGH);
  digitalWrite(gelb, LOW);
  delay(2000);
  digitalWrite(gelb, HIGH);
  delay(500);
  digitalWrite(rot, LOW);
  digitalWrite(gelb, LOW);
  digitalWrite(gruen, HIGH);
  delay(2000);
  digitalWrite(gelb,HIGH);
  digitalWrite(gruen, LOW);
  delay(500);
}
