#include <Servo.h>  // servo aufruf in Bibliothek


Servo servo1;  // servo Kontrolle


void setup()
{

  servo1.attach(9, 900, 2100);  // Verbindung zu Pin 9
               

}


void loop()
{
  int position;     // Deklaration der Position 
  

  servo1.write(90);    // Servo 90`C Ausrichtung 

  delay(1000);         // Verzögerung für die Drehung

  servo1.write(180);   // servo 180`C Ausrichtung

  delay(1000);         // Verzögerung für die Drehung

  servo1.write(0);     // Servo 0´C Ausrichtung 

  delay(1000);         // Verzögerung für die Bewegung
  
 
 
  for(position = 0; position < 180; position += 2)
  {
    servo1.write(position);  // Wechsel zur nächsten Position
    delay(20);               // Verzögerung zur Bewegung 
  }

  for(position = 180; position >= 0; position -= 1)
  {                                
    servo1.write(position);  // Wechsel zur nächsten Position
    delay(20);               // Verzögerung zur Bewegung
  }
}
